import "./App.css";
import { BrowserRouter, Switch, Route, Link } from "react-router-dom";

import HomeCinema from "./components/HomeCinema";
import Search from "./components/Search";


function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={HomeCinema}/>
        <Route path="/search" component={Search}/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;

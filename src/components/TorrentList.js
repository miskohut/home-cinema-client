import React from 'react';
import List from '@material-ui/core/List';
import Torrent from './Torrent';

export default class TorrentList extends React.Component {
    render() {
        return ( 
            <List>
                {
                    this.props.data.map((torrent ,i) => {
                        return <Torrent id={i} data={torrent} />;
                    })
                }
            </List> 
        );
    }
}
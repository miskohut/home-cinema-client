import React from "react";
import { AppBar, IconButton, List, ListItem, Toolbar } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import TextField from "@material-ui/core/Input";
import ListItemText from "@material-ui/core/ListItemText";

export default class Search extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: "",
      results: [],
    };
  }

  onTextChanged = (e) => {
    this.setState({
      searchText: e.target.value,
    });
  };

  search = async () => {
    let options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ search: this.state.searchText }),
    };

    let response = await fetch("http://192.168.0.155:3001/search", options);
    await this.setState({
      results: await response.json(),
    });

  };

  downloadTorrent = (index) => {
    let result = this.state.results[index];
    if (result) {
      let options = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ title: result.title }),
      };

      fetch("http://192.168.0.155:3001/download", options);
      this.props.history.goBack();
    }
  };

  enterPressed = (event) => {
    var code = event.keyCode || event.which;
    if(code === 13) { //13 is the enter keycode
      this.search();
    } 
  }

  render() {
    return (
      <div>
        <AppBar position="static">
          <Toolbar>
            <TextField
              id="standard-basic"
              label="Standard"
              onChange={this.onTextChanged}
              onKeyPress={this.enterPressed.bind(this)}
            />
            <IconButton onClick={this.search}>
              <SearchIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <List>
          {this.state.results.map((result, i) => {
            return (
              <ListItem key={i} button onClick={() => this.downloadTorrent(i)}>
                <ListItemText primary={result.title} />
              </ListItem>
            );
          })}
        </List>
      </div>
    );
  }
}

import React from "react";
import ListItem from "@material-ui/core/ListItem";
import Card from "@material-ui/core/Card";
import { CardContent, IconButton, Typography } from "@material-ui/core";
import ProgressBar from "react-bootstrap/ProgressBar";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import PauseIcon from "@material-ui/icons/Pause";
import ClearIcon from "@material-ui/icons/Clear";

import styles from "./styles/Torrent.css";

export default class Torrent extends React.Component {
  constructor(props) {
    super(props);

    this.styles = new Styles();
    this.state = { state: this.props.data.state };
  }

  pause = () => {
    let options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ magnet: this.props.data.magnet }),
    };

    if (this.state.state === 1) {
      fetch("http://192.168.0.155:3001/pause", options);
      this.setState({ state: 2 });
    } else {
      fetch("http://192.168.0.155:3001/resume", options);
      this.setState({ state: 1 });
    }
  };

  get writeDownloaded() {
    let bytes = this.props.data.downloaded;
    var sizes = ["Bytes", "KB", "MB", "GB", "TB"];
    if (bytes === 0) return "0 Byte";
    var i = parseFloat(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (
      Math.round((bytes / Math.pow(1024, i) + Number.EPSILON) * 100) / 100 +
      " " +
      sizes[i]
    );
  }

  get writeFileSize() {
    let bytes = this.props.data.fileSize;
    var sizes = ["Bytes", "KB", "MB", "GB", "TB"];
    if (bytes === 0) return "0 Byte";
    var i = parseFloat(Math.floor(Math.log(bytes) / Math.log(1024)));
    return (
      Math.round((bytes / Math.pow(1024, i) + Number.EPSILON) * 100) / 100 +
      " " +
      sizes[i]
    );
  }

  removeTorrent = () => {
    let options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ magnet: this.props.data.magnet }),
    };

    fetch("http://192.168.0.155:3001/remove", options);
  };

  render() {
    return (
      <ListItem width="100%" class={styles.listItem}>
        <Card class={styles.card}> 
            <CardContent>

              <div class={styles.name}>
                <Typography>{this.props.data.name}</Typography>

                <IconButton onClick={this.removeTorrent}>
                  <ClearIcon />
                </IconButton>
              </div>

              <ProgressBar now={this.props.data.progress} variant="info" />

              <div>
                  {this.writeDownloaded} / {this.writeFileSize}
              </div>
              <div>
                <IconButton onClick={this.pause}>
                  {this.props.data.state === 1 ? (
                    <PauseIcon />
                  ) : (
                    <PlayArrowIcon />
                  )}
                </IconButton>
              </div>
            </CardContent>
        </Card>
      </ListItem>
    );
  }
}

class Styles {
  get cardStyle() {
    return {};
  }

  get right() {
    return {
      float: "right",
    };
  }

  get removeButton() {
    return {
      float: "right",
    };
  }
}

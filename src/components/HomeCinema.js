import { AppBar, IconButton, Toolbar } from '@material-ui/core';
import React from 'react';
import TorrentList from './TorrentList';
import AddIcon from '@material-ui/icons/Add';


export default class HomeCinema extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            data: [],
            dialogOpened: false
        };
        this.refresh();

        console.log(process.env.mockup)
    }

    refresh() {
        setInterval(() => {
            fetch('http://192.168.0.155:3001/').then(response => response.json()).then(data => this.setState({data: data}));
        }, 1000);
    }

    addTorrent = () => {
        this.props.history.push("/search");
    }

    close = () => {
        this.setState({
            dialogOpened: false
        });
    }

    render() {
        return (
            <div>
                <AppBar position="static">
                    <Toolbar>
                        <IconButton onClick={this.addTorrent} to="/search">
                            <AddIcon style={{ color: "white" }} />
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <TorrentList data={this.state.data} />
            </div>
        );
    }
}